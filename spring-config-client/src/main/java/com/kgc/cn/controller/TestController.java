package com.kgc.cn.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by scy on 2020/1/7
 */

@RestController
@RequestMapping(value = "/test")
public class TestController {
    @Value("${test.config}")
    private String config;

    @GetMapping(value = "/config")
    public String test() {
        return config;
    }

}
