package com.kgc.cn;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class Kh73ConfigClientApplicationTests {
    @Value("${test.config}")
    private String config;

    @Test
    public void contextLoads() {

        System.out.println(config);
    }

}
