package com.kgc.cn.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;


/**
 * Created by scy on 2020/1/3
 */

@Data
@ToString
public class User implements Serializable {
    private static final long serialVersionUID = 8275992279073433764L;
    private String name;
    private int age;
}
