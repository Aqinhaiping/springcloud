package com.kgc.cn.controller;

import com.kgc.cn.model.User;
import org.springframework.web.bind.annotation.*;

/**
 * Created by scy on 2020/1/3
 */
@RestController
@RequestMapping(value = "/base")
public class ServiceController {

    @PostMapping(value = "/test")
    public String test(@RequestBody User user) {
        user.setName("service2");
        return user.toString();
    }
}
