package com.kgc.cn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Kh73ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Kh73ServiceApplication.class, args);
    }

}
